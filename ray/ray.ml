open Vec3

type ray = vec3 * vec3

let origin (o,_: ray) : vec3 = o
let direction (_,d: ray) : vec3 = d

let point_at (o,d: ray) (t: float) : vec3 =
  scmult d t |> add o
