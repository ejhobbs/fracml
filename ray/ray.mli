open Vec3
type ray = vec3 * vec3

val origin : ray -> vec3
val direction : ray -> vec3
val point_at : ray -> float -> vec3
