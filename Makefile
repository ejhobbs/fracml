default: build
build:
	dune build ./fracml.exe
run:
	dune exec -- ./fracml.exe
clean:
	dune clean
test:
	dune runtest
