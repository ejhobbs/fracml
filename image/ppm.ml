type pixel = float * float * float
type ppm = {
  n_cols: int; n_rows: int;
  n_colours: int;
  pos: int;
  data: bytes
}

let shade_to_int (n: int) (f: float): int =
  let s = float_of_int n +. 0.99 in
  s *. f |> int_of_float

let pp_colour (img: bytes) (offset: int) : string =
  let get_int i = Bytes.get img i |> int_of_char in
  let r = get_int offset in
  let g = get_int offset+1 in
  let b = get_int offset+2 in
  Printf.sprintf "%d %d %d \n" r g b

let rec pp_img (img: bytes) (pixel: int) : string =
  match pixel < Bytes.length img/3 with
  | false -> ""
  | true ->
    let c = pp_colour img (3*pixel) in
    pp_img img (pixel+1) |> Printf.sprintf "%s %s" c

let append_byte (byte: int) (img: ppm)  : ppm =
  let pos = img.pos in
  let bytes = img.data in
  char_of_int byte |> Bytes.set bytes pos;
  {img with data = bytes; pos = pos+1;}

(*****    INTERFACE    *****)

let string_of_ppm (img: ppm) : string =
  Printf.sprintf "P3\n%d %d\n%d\n%s"
      img.n_cols img.n_rows img.n_colours
      (pp_img img.data 0)

let new_img (size: int * int) (n_colours: int) : ppm =
  let n_cols,n_rows = size in
  {
    n_cols; n_rows; n_colours;
    data = char_of_int 0 |> Bytes.make (n_cols*n_rows*3);
    pos = 0;
  }

let add_pixel (img: ppm) (r,g,b: pixel) : ppm =
  let sti = shade_to_int img.n_colours in
  img |> append_byte (sti r)
      |> append_byte (sti g)
      |> append_byte (sti b)

let rec add_pixels (img: ppm) (p: pixel list) : ppm =
  match p with
  | [] -> img
  | p::p' -> add_pixels (add_pixel img p) p'
