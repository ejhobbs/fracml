type ppm
type pixel = float * float * float
val string_of_ppm : ppm -> string
val new_img : int*int -> int -> ppm
val add_pixel : ppm -> pixel -> ppm
val add_pixels : ppm -> pixel list -> ppm
