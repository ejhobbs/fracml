let width = 20
let height = 10
let depth = 255

let fdiv (a: int) (b: int) : float =
  float_of_int a /. float_of_int b

let rec gen_row (gen: int -> Ppm.pixel) (col: int): Ppm.pixel list =
  match col < width with
  | false -> []
  | true  -> (gen col)::gen_row gen (col+1)

let rec populate_img img row : Ppm.ppm =
  match row>=height with
  | true  -> img
  | false ->
    let pgen c = (fdiv c width, fdiv row height, 0.3) in
    let img' = gen_row (pgen) 0 |> List.rev in
    populate_img (Ppm.add_pixels img img') (row+1)

let test _ =
  let img = Ppm.new_img (width, height) depth in
  populate_img img 0
