exception InvalidIndex
type vec3 = float * float * float

val to_string: vec3 -> string

val x : vec3 -> float
val y : vec3 -> float
val z : vec3 -> float

val len: vec3 -> float

val scmult: vec3 -> float -> vec3
val scdiv: vec3 -> float -> vec3

val add: vec3 -> vec3 -> vec3
val sub: vec3 -> vec3 -> vec3
val mult: vec3 -> vec3 -> vec3
val div: vec3 -> vec3 -> vec3

val get_unit: vec3 -> vec3

val dot: vec3 -> vec3 -> float
val cross: vec3 -> vec3 -> vec3
