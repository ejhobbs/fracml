exception InvalidIndex
type vec3 = float * float * float

let to_string (x,y,z: vec3) : string =
  Printf.sprintf "%.3f, %.3f, %.3f" x y z
let%test _ = (0.1,0.2,0.3) |> to_string = "0.100, 0.200, 0.300"
let%test _ = (0.1234,0.2,0.3) |> to_string = "0.123, 0.200, 0.300"

let x (x,_,_: vec3): float = x
let y (_,y,_: vec3): float = y
let z (_,_,z: vec3): float = z
let%test _ = (0.1,0.2,0.3) |> x = 0.1
let%test _ = (0.1,0.2,0.3) |> y = 0.2
let%test _ = (0.1,0.2,0.3) |> z = 0.3

let sqr_len (x,y,z: vec3) : float =
  x*.x +. y*.y +. z*.z
let len (v: vec3) : float =
  sqr_len v |> sqrt
let%test _ = (0.0,0.0,0.0) |> len = 0.0
let%test _ = (1.0,0.0,0.0) |> len = sqrt 1.0
let%test _ = (1.0,0.0,3.0) |> len = sqrt 10.0
let%test _ = (1.0,2.0,3.0) |> len = sqrt 14.0

(**
 * Apply f to each item in vec3
 * returns new vec3 of result
 *)
let apply (f: float -> float) (x,y,z: vec3) : vec3 =
  f x, f y, f z
let%test _ = (1.0,2.0,3.0) |> apply (( *.) 2.0) = (2.0,4.0,6.0)
let%test _ = (1.0,2.0,3.0) |> apply (( +.) 5.0) = (6.0,7.0,8.0)

(* scalar multiply/divide *)
let scmult (v: vec3) (m: float) : vec3 =
  let mult f = f*.m in
  apply mult v

let scdiv (v: vec3) (m: float) : vec3 =
  let div f = f/.m in
  apply div v

(* basic ops *)
let add (x1,y1,z1: vec3) (x2,y2,z2: vec3): vec3 =
  x1+.x2, y1+.y2, z1+.z2

let%test _ = add (1.,2.,1.) (3.,4.,1.) = (4.,6.,2.)

let sub (x1,y1,z1: vec3) (x2,y2,z2: vec3): vec3 =
  x1-.x2, y1-.y2, z1-.z2

let%test _ = sub (1.,2.,1.) (3.,4.,1.) = (-2.,-2.,0.)

let mult (x1,y1,z1: vec3) (x2,y2,z2: vec3): vec3 =
  x1*.x2, y1*.y2, z1*.z2

let div (x1,y1,z1: vec3) (x2,y2,z2: vec3): vec3 =
  x1/.x2, y1/.y2, z1/.z2

(* other ops *)
let get_unit (v: vec3) : vec3 =
   scdiv v (len v)

let dot (x1,y1,z1: vec3) (x2,y2,z2: vec3): float =
  (x1*.x2) +. (y1*.y2) +. (z1+.z2)

let cross (x1,y1,z1: vec3) (x2,y2,z2: vec3): vec3 =
  (y1*.z2)-.(z1*.y2),
  -.((x1*.z2)-.(z1*.x2)),
  (x1*.y2)-.(y1*.x2)
