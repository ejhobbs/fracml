type row_gen = int -> Ppm.pixel list

let llc         = (-2., -1., -1.)
let horizontal  = (4., 0., 0.)
let vertical    = (0., 2., 0.)
let origin      = (0., 0., 0.)

let width = 200
let height = 100
let colors = 255

let ( /~ ) a b = (float_of_int a)/.(float_of_int b)
let ( +~ ) a b = Vec3.add a b
let ( -~ ) a b = Vec3.sub a b
let ( *~ ) a b = Vec3.scmult a b
let sqr f = f*.f

let hit (radius: float) (center: Vec3.vec3) (origin, direction: Ray.ray) : bool =
  let oc = origin -~ center in
  let a = Vec3.dot direction direction in
  let b = 2. *. Vec3.dot oc direction in
  let c = Vec3.dot oc oc -. (sqr radius) in
  let dis = (sqr b) -. 4.*.a*.c in
  dis > 0.

let color (base: Vec3.vec3) (r : Ray.ray): Vec3.vec3 =
  let hit_point = hit 0.5 (0.,0.,-1.) r in
  match hit_point with
  | true -> 1.,0.,0.
  | false ->
    let unit_dir = Ray.direction r |> Vec3.get_unit in
    let t = 0.5*.(Vec3.y unit_dir +. 1.0) in
    ((0.2,0.7,0.1) *~ (1.0-.t)) +~ (base *~ t)

let rec row (c: int) (r: int): Ppm.pixel list =
  match c < width with
  | false -> []
  | true ->
    let u = c /~ width in
    let v = r /~ height in
    (* u/v tend toward 1.0 *)
    ((origin, llc +~ horizontal *~ u +~ vertical *~ v)
    |> color (1.0, 0.4, 1.0))::row (c+1) r

let rec each_row (img: Ppm.ppm) (r: int) : Ppm.ppm =
  match r>=height with
  | true -> img
  | false -> each_row (Ppm.add_pixels img (row 0 r)) (r+1)

let write_to_file (n: string) (s: string) : unit =
  let file = open_out n in
  output_string file s;
  close_out file

let _ =
(*  let img = Ppm.new_img (width,height) colors in
  each_row img 0*)
  Ppmtest.test ()
  |> Ppm.string_of_ppm
  |> write_to_file "sky.ppm"
